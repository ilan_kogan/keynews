const express = require("express");
const bodyParser = require("body-parser");
const {manager} = require('./TheSender/index');
const app = express();

const cors=require('cors');
app.use(cors());

const port = 3002;

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`));

setInterval(manager, 900000);
