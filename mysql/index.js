const {mysqlConfig} = require('config');
var mysql = require('mysql');
var pool  = mysql.createPool({
    connectionLimit : 10,
    host            : mysqlConfig.host,
    user            : mysqlConfig.user,
    password        : mysqlConfig.password,
    database        : mysqlConfig.database
});

// pool.query('SELECT 1 + 1 AS solution', function (error, results, fields) {
//     if (error) throw error;
//     console.log('The solution is: ', results[0].solution);
// });


module.exports.queryHandler = async function(query){
    const results = await new Promise((resolve, reject) => {
        pool.getConnection(async function (err, connection) {
            if(err) reject(err);
            connection.query(query, function (error, results, fields) {
                connection.release();
                if(error) reject(error);
                resolve(results);
            })
        })
    });
    return results;
};

