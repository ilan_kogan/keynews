const getUsersKeywords = require('../../mysql/queries/getAllUsersKeywords');
const {queryHandler} = require('../../mysql/index');

module.exports = async function(userId){
    return await queryHandler(getUsersKeywords(userId));
};
