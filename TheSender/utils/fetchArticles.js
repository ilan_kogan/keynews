const {newsApiKey} = require('config');
const axios = require('axios');
const moment = require('moment');

module.exports = async function (keyword, language) {
    var yesterday = moment().subtract(1, "days").format("YYYY-MM-DD");
    const uri = `https://newsapi.org/v2/everything?q=${keyword}&apiKey=${newsApiKey}&from=${yesterday}&sortBy=publishedAt&language=${language}`;
    const encodedUri = encodeURI(uri);
    const result = await axios.get(encodedUri);
    return result.data.articles
};
