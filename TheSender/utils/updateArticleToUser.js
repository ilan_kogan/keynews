const updateArticleToUser = require('../../mysql/queries/updateArticleToUser');
const { queryHandler } = require('../../mysql/index');

module.exports = async function (link, author, title, description, imageLink, piblishedAt, userId) {
    return await queryHandler(updateArticleToUser(link, author, title, description, imageLink, piblishedAt, userId));
};
