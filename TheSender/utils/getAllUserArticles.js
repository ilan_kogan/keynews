const getAllUserArticles = require('../../mysql/queries/getUserArticles');
const {queryHandler} = require('../../mysql/index');

module.exports = async function(userId){
    return await queryHandler(getAllUserArticles(userId));
};
