const checkLinkToUser = require('../../mysql/queries/checkLinkToUser');
const {queryHandler} = require('../../mysql/index');

module.exports = async function(userId, link){
    return await queryHandler(checkLinkToUser(userId, link));
};
