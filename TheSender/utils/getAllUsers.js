const getAllUsers = require('../../mysql/queries/getAllUsers');
const {queryHandler} = require('../../mysql/index');

module.exports = async function(){
    return await queryHandler(getAllUsers());
};
