const getAllUsers = require('./utils/getAllUsers');
const getUserKeywords = require('./utils/getUsersKeywords');
const fetchArticles = require('./utils/fetchArticles');
const checkLinkToUser = require('./utils/checkLinkToUser');
const addArticleToUser = require('./utils/updateArticleToUser');
const fixString = require('../helpers/fixString')
const nodemailer = require("nodemailer");

async function generateHtml(articles) {
    let string = '';
    for (article of articles) {
        string += `<li>
                        <ul>
                            <li>title: ${article.title}</li>
                            <li>link to article: ${article.url}</li>
                            <li>description: ${article.description}</li>
                            <li>date published: ${article.publishedAt}</li>
                        </ul>
                    </li><br>`
    }
    return string;
}


async function sendEmail(articles, email) {
    let mailerConfig = {
        host: "smtp.office365.com",
        secureConnection: true,
        port: 587,
        auth: {
            user: "ilan@beam.travel",
            pass: "Pencilcup123"
        }
    };
    let transporter = nodemailer.createTransport(mailerConfig);

    let mailOptions = {
        from: mailerConfig.auth.user,
        to: email,
        subject: 'articles that are related to the keywords you specified',
        html: `<ul>${await generateHtml(articles)}</ul>`
    };

    transporter.sendMail(mailOptions, function (error) {
        if (error) {
            console.log('error:', error);
        }
    });
}

async function updateArticleToUser(url, userId, author, title, description, imageLink, publishedAt) {
    const isLinkToUserExists = (await checkLinkToUser(userId, url)).length !== 0;
    if (!isLinkToUserExists) {
        await addArticleToUser(url, author, title, description, imageLink, publishedAt, userId);
    }
    return isLinkToUserExists;
}

async function startFetchingProccess(user) {
    const keywords = await getUserKeywords(user.Id);
    let articlesToSend = [];
    for (let i = 0; i < keywords.length; i++) {
        let articlesFromApi = await fetchArticles(keywords[i].Value, keywords[i].Language);
        for (let i = articlesFromApi.length - 1; i >= 0; i--) {
            const article = articlesFromApi[i]
            const isLinkSentAleardy = await updateArticleToUser(article.url, user.Id, fixString(article.author), fixString(article.title), fixString(article.description), article.urlToImage, article.publishedAt);
            if (!isLinkSentAleardy) {
                articlesToSend.push(article)
            }
        }
    }
    if (articlesToSend.length > 0) {
        await sendEmail(articlesToSend, user.Email);
    }
}

module.exports.manager = async function () {
    const allUsers = await getAllUsers();
    for (let i = 0; i < allUsers.length; i++) {
        try {
            await startFetchingProccess(allUsers[i])
        }
        catch (error) {
            console.log(`error sending email to ${allUsers[i].Email} ${error}`)
        }
    }
    if (allUsers.length === 0) {
        console.log('no email was sent, 0 users in db')
    }
    else {
        console.log('finished sending email to all users')
    }
};
