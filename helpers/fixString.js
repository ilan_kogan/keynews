module.exports = (str) => {
    if (str)
        return str.replace(/[',"]/g, '');
    return null;
}